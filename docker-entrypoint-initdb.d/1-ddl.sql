CREATE TABLE `user_role`
(
    `user_id` BIGINT(20) UNSIGNED NOT NULL,
    `role_id` INT(10) UNSIGNED NOT NULL,
    PRIMARY KEY (`user_id`, `role_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `user_feature`
(
    `user_id`    BIGINT(20) UNSIGNED NOT NULL,
    `feature_id` INT(10) UNSIGNED NOT NULL,
    PRIMARY KEY (`user_id`, `feature_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE INDEX idx_user_role ON user_role (role_id);
