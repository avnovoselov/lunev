<?php

$dsn = "mysql:host=127.0.0.1;dbname=lunev;charset=utf8mb4";

$options = [
    PDO::ATTR_EMULATE_PREPARES   => false,
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
];

$c = new PDO($dsn, "root", "lunev", $options);

$roles          = range(1, 30);
$users          = range(1, 3000);
$features       = range(1, 5000);
$usersPerRole   = 300;
$featurePerUser = 100;

$statement = $c->prepare('insert into user_role set role_id = ?, user_id = ?');
foreach ($roles as $role) {
    $map = [];
    foreach (range(1, $usersPerRole) as $_) {
        $pair = [$role, $users[rand(0, count($users) - 1)]];

        $map[implode('_', $pair)] = $pair;
    }

    foreach ($map as $pair) {
        $statement->execute($pair);
    }
}
echo sprintf("%sRoles was created", PHP_EOL);

$statement = $c->prepare('insert into user_feature set feature_id = ?, user_id = ?');
foreach ($users as $user) {
    $map = [];
    foreach (range(1, $featurePerUser) as $_) {
        $pair = [$features[rand(0, count($features) - 1)], $user];

        $map[implode('_', $pair)] = $pair;
    }

    foreach ($map as $pair) {
        $statement->execute($pair);
    }
}

echo sprintf("%sFeatures was created%s", PHP_EOL, PHP_EOL);
